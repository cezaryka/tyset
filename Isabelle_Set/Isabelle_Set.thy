theory Isabelle_Set
imports
  Sets
  Function
  Monops
  Ordinal
  String
  Object
  Structures
  Monoid
  Ring
  Set_Extension
  Nat
  Integer
  (* Category *)

begin

text \<open>Just an entry point to load everything.\<close>

end
